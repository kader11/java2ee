package com.example.web;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.dao.MonumentDAO;
import com.example.demo.entities.Monument;

@Controller
@RequestMapping("/monument")
public class MonumentController {
	@Autowired
	private MonumentDAO moda;
	@RequestMapping(value = "montListe", method = RequestMethod.GET)
	public String afficheMonList(Model model) {
		List<Monument> lmx =moda.findAll();
		model.addAttribute("monListe", lmx);
		return "montListe";
	}
	
	/*
	@RequestMapping(value="/SuppMonu", method = RequestMethod.GET)
	public String afficheMon(Model model) {
		List<Monument> lmx = moda.findAll();
		model.addAttribute("listeM", lmx);
		return "suppMonument";
	}
	@RequestMapping(value="/supprimer", method = RequestMethod.GET)
	public String deleteLieu(String codeM) {
		moda.supp(codeM);
		return "redirect:SuppMonu";
	}
	
	
	@RequestMapping(value ="/formulaireMonument", method = RequestMethod.GET)
	public String FormAddM(Model model) {
		model.addAttribute("monumentForm", new Monument());
		return "AddMonument";
	}
	
	@PostMapping(value="/AddMonu")
	public String AddM(Model model, @ModelAttribute("monumentForm") Monument monumentAAjouter) {
		System.out.print(monumentAAjouter);
		
		moda.save(monumentAAjouter);
		List<Monument> lmx=moda.findAll();
		model.addAttribute("listeM", lmx);
		return "montListe";
	
	}
	/*
	@RequestMapping(value="SuppMonument", method = RequestMethod.GET)
	public String AfficheMonument(Model model) {
		List<Monument> mnt=moda.findAll();
		model.addAttribute("listeM", mnt);
		return "suppMonument";
	}
	@RequestMapping(value="/supprimer", method = RequestMethod.GET)
	public String deleteLieu(String codeM) {
		moda.supp(codeM);
		return "redirect:SuppMonument";
	}
	*/
	

}
