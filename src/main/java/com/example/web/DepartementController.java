package com.example.web;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;


import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.dao.departementDao;
import com.example.demo.entities.departement;


@Controller
@RequestMapping("/departement")
public class DepartementController {
	
	@Autowired
	private departementDao DepartementDAO;
	
	/*
	@RequestMapping(value ="", method = RequestMethod.GET)
	public String navCreate() {		
		return "index";
	}
	*/
	
	@RequestMapping(value ="/deptList")
	public String afficheDepListe(Model model) {
		
		List<departement> dps = DepartementDAO.findAll();
		model.addAttribute("deptList", dps);
		return "deptList";
	}
	
	/*
	@RequestMapping(value ="/formulaireDepartement", method = RequestMethod.GET)
	public String addDPT(Model model) {
		model.addAttribute("depForm", new departement());
		return "AddDept";
	}
	*/
	
}

