package com.example.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.dao.CelebriteDao;
import com.example.demo.entities.Celebrite;



@Controller
@RequestMapping("/celebrite")
public class CelebriteController {
    @Autowired
    private CelebriteDao celebriteDAO;
    
   /* 
    @RequestMapping(value ="", method = RequestMethod.GET)
    public String navCreate() {        
        return "index.html";
        
    }*/
    /*Ici mon objet se nomme cls mais j'ai nommé par la suite clts l'attribut
    qui contient cet objet dans la requête. Côté vue,
    c'est par ce nom d'attribut que je pourrai accéder à mon objet !*/
    
    
    
    @RequestMapping(value ="/ListCeleb", method = RequestMethod.GET)
    public String afficheCelListe(Model model) {
        
        List<Celebrite> cls = celebriteDAO.findAll();
        model.addAttribute("clts", cls);
        return "ListCeleb";
   }
    
    
    
    @RequestMapping(value ="/formulaireCelebrite", method = RequestMethod.GET)
    public String addCLB(Model model) {
        model.addAttribute("celebForm", new Celebrite());
        return "AddCeleb";
        
    }
    
    @PostMapping(value ="AddCeleb")
    public String AddCLB(Model model, @ModelAttribute("celebForm") Celebrite celAAjouter) {
        celebriteDAO.save(celAAjouter);
        
        List<Celebrite> cls = celebriteDAO.findAll();
        model.addAttribute("clts", cls);
        return "listCelebrite";
        
        
    }
    
}



