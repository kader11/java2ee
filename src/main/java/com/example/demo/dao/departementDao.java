package com.example.demo.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.example.demo.entities.departement;



@Repository
@Transactional
@Service
public class departementDao implements EntityRepository<departement> {
	

	
	@PersistenceContext
	private EntityManager entityManager;
	
	public departement save(departement a) {
		entityManager.persist(a);
		return a;
	}
	
	@Override
	public List<departement> findAll(){
		Query req=entityManager.createQuery("select p from departement p");
		return req.getResultList();
	}
	
	@Override
	public List<departement> findByDesignation(String am) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void remove(departement a) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void supp(Integer dep) {
		// TODO Auto-generated method stub
		departement d = entityManager.find(departement.class, dep);
		entityManager.remove(d);       
	}
	@Override
	public departement update(departement a) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void supp(String a) {
		// TODO Auto-generated method stub 
		
	}

	@Override
	public departement get(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}
}
