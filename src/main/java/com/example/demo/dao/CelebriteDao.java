package com.example.demo.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.example.demo.entities.Celebrite;





@Repository
@Transactional
@Service
public class CelebriteDao implements EntityRepository<Celebrite> {
	@PersistenceContext
	private EntityManager em;
  
	
	@Override
	public Celebrite save(Celebrite a) {
		em.persist(a);
		return a;
	}
	
	@Override
	public List<Celebrite> findAll() {
		Query req=em.createQuery("select p from Celebrite p");
		return req.getResultList();
			
	}
	
	
	@Override
	public List<Celebrite> findByDesignation(String am) {
		// TODO Auto-generated method stub
		return null;
	}


	
		
	@Override
	public void remove(Celebrite a) {
		Celebrite d = em.find(Celebrite.class, a);
		em.remove(d);
	}

	@Override
	public void supp(Integer a) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Celebrite update(Celebrite a) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void supp(String a) {
		// TODO Auto-generated method stub
		
	}

	
	public Celebrite get(Integer num_Celebrite) {
		// TODO Auto-generated method stub
		
		Celebrite c = em.find(Celebrite.class, num_Celebrite);
		return c;

	
}
}
