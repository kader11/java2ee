package com.example.demo.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.stereotype.Service;


import com.example.demo.entities.departement;
import com.example.demo.entities.Lieu;
import com.example.demo.entities.Monument;

@Transactional
@Service
public class LieuDao implements EntityRepository<Lieu> {
	@PersistenceContext
	private EntityManager em;

        public Lieu get(String codeInsee) {
		
		return em.find(Lieu.class, codeInsee);
	}
        
	public List<Lieu> findAll(){
		Query req=em.createQuery("select p from Lieu p");
		return req.getResultList();
	}
	
	
	@Override
	
	public void supp(Integer code_insee) {
		// TODO Auto-generated method stub
		Lieu d = em.find(Lieu.class, code_insee);
		em.remove(d);
		
	}
	
	
	@Override
	public Lieu get(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	@Override

	public Lieu save(Lieu a) {
		em.persist(a);
		return a;
	}
	
	public void supp(String code_insee) {
		Lieu l = em.find(Lieu.class, code_insee);
				em.remove(l);
	}
	
	@Override
	public List<Lieu> findByDesignation(String am) {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public void remove(Lieu a) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public Lieu update(Lieu a) {
		// TODO Auto-generated method stub
		return null;
	}	
	}
	
		
	
		

