package com.example.demo.dao;

import java.util.List;

public interface EntityRepository<T> {
	public T get(Integer id);
	public T save(T a);		
	public List<T> findAll();
	public List<T> findByDesignation(String am);	
	public void remove(T a);	
	public void supp(Integer a);
	public T update(T a);
	void supp(String a);

	
}



