package com.example.demo.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.example.demo.entities.Utilisateur;





	@Transactional
	@Service
	public class UtilisateurDao {
		
		@PersistenceContext
		private EntityManager entityManager;
			
		
		
	    public Utilisateur getUser(Utilisateur userReceived) {
	    	Utilisateur newLoggedUser = new Utilisateur();
	            newLoggedUser = entityManager.find(Utilisateur.class, userReceived.getEmail());
	            if (newLoggedUser == null) {
	                System.out.println("le champ est null");

	                return null;
	            }
	                       
	            if (!userReceived.getPassword().equals(newLoggedUser.getPassword())) {

	                return null;
	            }
	            else return newLoggedUser;
	           
	    }

	    
	    public Utilisateur addUtilisateur(Utilisateur newUser) {
	    	Utilisateur userExistant= entityManager.find(Utilisateur.class, newUser.getEmail());
	        if (userExistant!=null) {
	            throw new RuntimeException("Ce compte éxiste déjà");
	        }
	        else {
	        	entityManager.persist(newUser);
	            return newUser;
	        }           
	    }

	}
