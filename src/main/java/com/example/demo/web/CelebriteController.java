package com.example.demo.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.demo.dao.CelebriteDao;
import com.example.demo.entities.Celebrite;




@Controller
@RequestMapping("/Celebrite")
public class CelebriteController {
    @Autowired
    private CelebriteDao celebriteDAO;
    
 
    
    
    
    @RequestMapping(value ="/ListCeleb")
    public String afficheCelListe(Model model) {
        
        List<Celebrite> cls = celebriteDAO.findAll();
        model.addAttribute("ListCeleb", cls);
        return "ListCeleb";
   }
    
   
    
    @RequestMapping(value ="/formCeleb", method = RequestMethod.GET)
	public String addCLB(Model model) {
		model.addAttribute("celebForm", new Celebrite());
		return "AddCeleb";
	
		
	}
	@PostMapping(value ="/AddCeleb")
	public String AddCLB(Model model, @ModelAttribute("celebForm") Celebrite celAAjouter) {
		celebriteDAO.save(celAAjouter);
		
		
		
	
        List<Celebrite> cls = celebriteDAO.findAll();
        model.addAttribute("clts", cls);
        return "redirect:ListCeleb";
        
        
    }
	
	@RequestMapping(value ="/SuppCeleb", method = RequestMethod.GET)
	public String afficheDepListes(Model model) {
		
		List<Celebrite> cls = celebriteDAO.findAll();
		model.addAttribute("sup", cls);
		return "SuppCeleb";
	}
	
	@RequestMapping(value="/supprimer", method = RequestMethod.GET)
	public String deletedep( Integer num_Celebrite) {
		celebriteDAO.supp(num_Celebrite);
		
		return "redirect:SuppCeleb";
	}

}

    




