package com.example.demo.web;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.demo.dao.MonumentDAO;
import com.example.demo.entities.Monument;
import com.example.demo.entities.departement;

@Controller
@RequestMapping("/monument")
public class MonumentController {
	@Autowired
	private MonumentDAO moda;
	@RequestMapping(value = "MonList", method = RequestMethod.GET)
	public String afficheMonList(Model model) {
		List<Monument> lmx =moda.findAll();
		model.addAttribute("listeM", lmx);
		return "MonList";
	}
	
	
	@RequestMapping(value="/SuppMonu", method = RequestMethod.GET)
	public String afficheMon(Model model) {
		List<Monument> lmx = moda.findAll();
		model.addAttribute("listeM", lmx);
		return "suppMonument";
	}
	
	@RequestMapping(value="/supprimer", method = RequestMethod.GET)
	public String deleteLieu(String codeM) {
		moda.supp(codeM);
		return "redirect:SuppMonu";
	}
	
	
	
	
	@RequestMapping(value ="/formulaireMonument", method = RequestMethod.GET)
	public String FormAddM(Model model) {
		model.addAttribute("monumentForm", new Monument());
		return "AddMonument";
	}
	
	@PostMapping(value="/AddMonument")
	public String AddM(Model model, @ModelAttribute("monumentForm") Monument monumentAAjouter) {
		System.out.print(monumentAAjouter);
		moda.save(monumentAAjouter);
		
		
		List<Monument> lmx=moda.findAll();
		model.addAttribute("listeM", lmx);
		
		return "MonList";
		
		
		
			
	}
}
 




