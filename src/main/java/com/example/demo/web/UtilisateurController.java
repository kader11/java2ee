package com.example.demo.web;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.demo.dao.UtilisateurDao;
import com.example.demo.entities.Utilisateur;
import com.example.demo.security.AccesSecurity;

@Controller
public class UtilisateurController {
	@Autowired
	private UtilisateurDao utilisateurDao;
	private AccesSecurity accessSecurity = new AccesSecurity();
	
	@RequestMapping(value ="/", method = RequestMethod.GET)
	public String navCreate() {		
		return "index";
	}

	
	
	@RequestMapping(value="/login")
	public String loginDefault(Model model){
		Utilisateur user = new Utilisateur();
		model.addAttribute("userForm", user);
		return "Login";
	}
	
	
	@RequestMapping(value="/accueil", method = RequestMethod.POST)
	public String checkLogin(@ModelAttribute("registerForm") Utilisateur userReceived, HttpSession session, Model model){
		boolean error = false;
		Utilisateur logUser = utilisateurDao.getUser(userReceived);   
		if (logUser != null) {
			session.setAttribute("USER", logUser);
			return "accueil";
		}
		error = true;
		model.addAttribute("error", error);
		
		
		return "Login";
	}
	
	

	@RequestMapping(value ="/register", method = RequestMethod.GET)
	public String addDPT(Model model) {
		model.addAttribute("registerForm", new Utilisateur());
		return "SignUP";
	}
	
	@PostMapping(value ="/SignUP")
	public String addDPT(Model model, @ModelAttribute("registerForm") Utilisateur newUser) {
		utilisateurDao.addUtilisateur(newUser);		
		return "redirect:/login";
	}



	
	

}
