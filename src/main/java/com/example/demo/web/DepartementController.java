package com.example.demo.web;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.demo.dao.departementDao;
import com.example.demo.entities.departement;



@Controller
@RequestMapping("/departement")
public class DepartementController {
	
	@Autowired
	private departementDao DepartementDAO;
	
	
	@RequestMapping(value ="", method = RequestMethod.GET)
	public String navCreate() {		
		return "index";
	}

	
	
	@RequestMapping(value ="/deptList")
	public String afficheDepListe(Model model) {
		List<departement> dps = DepartementDAO.findAll();
		model.addAttribute("deptList", dps);
		return "deptList";
	}
	
	
	
	@RequestMapping(value ="/formeDept", method = RequestMethod.GET)
	public String addDPT(Model model) {
		model.addAttribute("depForm", new departement());
		return "AddDept";
	}
	
	@PostMapping(value ="/AddDept")
	public String addDPT(Model model, @ModelAttribute("depForm") departement depAAjouter) {
		DepartementDAO.save(depAAjouter);
		
		
		
		List<departement> dps = DepartementDAO.findAll();
		model.addAttribute("dpts", dps);
		return "redirect:deptList";
	}
	
	
	
	
	
	@RequestMapping(value ="/SuppDept", method = RequestMethod.GET)
	public String afficheDepListes(Model model) {
		
		List<departement> dps = DepartementDAO.findAll();
		model.addAttribute("dptss", dps);
		return "supDept";
	}
	
	@RequestMapping(value="/supprimer", method = RequestMethod.GET)
	public String deletedep( Integer dep) {
		DepartementDAO.supp(dep);
		
		return "redirect:SuppDept";
	}
	
	
	
	
	
	
		
}

