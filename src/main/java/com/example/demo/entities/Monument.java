package com.example.demo.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "monument")
public class Monument implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	private String codeM;
	
	@Column(name = "nomM ")	
	private String nomM ;
	
	@Column(name = "proprietaire")
	private String proprietaire;
	
	@Column(name = "typeMonument")
	private String typeMonument;
	
	@Column(name = "longitude")
	private float longitude;
	
	@Column(name = "latitude")
	private float latitude;	
	
	
	
	@ManyToOne //(cascade = CascadeType.ALL, targetEntity = lieu.class)
	@JoinColumn(name="codeLieu", referencedColumnName ="code_insee")
	private Lieu codeLieu;
	
	@OneToMany(mappedBy = "codeM", fetch = FetchType.LAZY)
	private List<AssocieA> AssocieS = new ArrayList<AssocieA>();
	
	
	public Monument() {
		super();
	}
	
	public Monument(String codeM, String nomM, String proprietaire, String typeMonument, float longitude,
				float latitude, Lieu codeLieu, List<AssocieA> associeS) {
			super();
			this.codeM = codeM;
			this.nomM = nomM;
			this.proprietaire = proprietaire;
			this.typeMonument = typeMonument;
			this.longitude = longitude;
			this.latitude = latitude;
			this.codeLieu = codeLieu;
			AssocieS = associeS;
		}


	public String getCodeM() {
		return codeM;
	}


	public void setCodeM(String codeM) {
		this.codeM = codeM;
	}


	public String getNomM() {
		return nomM;
	}


	public void setNomM(String nomM) {
		this.nomM = nomM;
	}


	public String getProprietaire() {
		return proprietaire;
	}


	public void setProprietaire(String proprietaire) {
		this.proprietaire = proprietaire;
	}




	public float getLongitude() {
		return longitude;
	}


	public void setLongitude(float longitude) {
		this.longitude = longitude;
	}


	public float getLatitude() {
		return latitude;
	}


	public void setLatitude(float latitude) {
		this.latitude = latitude;
	}


	public String getTypeMonument() {
		return typeMonument;
	}


	public void setTypeMonument(String typeMonument) {
		this.typeMonument = typeMonument;
	}


	public Lieu getCodeLieu() {
		return codeLieu;
	}


	public void setCodeLieu(Lieu codeLieu) {
		this.codeLieu = codeLieu;
	}

	
	
	public List<AssocieA> getAssocieS() {
		return AssocieS;
	}

	public void setAssocieS(List<AssocieA> associeS) {
		AssocieS = associeS;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	

	
	

}
