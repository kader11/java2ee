package com.example.demo.entities;

import java.io.Serializable;
import java.util.ArrayList;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "lieu")
public class Lieu implements Serializable {
	
	//private static final long serialVersionUID = 1L;
	
	@Id
	private String code_insee;	
	
	@Column(name="latitude")
	private float latitude;
	
	@Column(name="longitude")
	private float longitude;
	
	@Column(name="nom_commune", length=46)
	private String nom_commune;
	
	@ManyToOne
	@JoinColumn(name ="dep", referencedColumnName="dep")
	private departement dep;
	
	
	@OneToMany(mappedBy = "codeLieu", fetch = FetchType.LAZY)
	private List<Monument> monuments= new ArrayList<Monument>();	
	
	
	public Lieu() {}
	
	public Lieu(String code_insee, float latitude, float longitude, String nom_commune, departement dep) {
		
		this.code_insee = code_insee;
		this.latitude = latitude;
		this.longitude = longitude;
		this.nom_commune = nom_commune;
		this.dep = dep;
	}






	public String getCode_insee() {
		return code_insee;
	}




	public void setCode_insee(String code_insee) {
		this.code_insee = code_insee;
	}




	public departement getDep() {
		return dep;
	}




	public void setDep(departement dep) {
		this.dep = dep;
	}




	public String getNom_commune() {
		return nom_commune;
	}




	public void setNom_commune(String nom_commune) {
		this.nom_commune = nom_commune;
	}




	public float getLatitude() {
		return latitude;
	}




	public void setLatitude(float latitude) {
		this.latitude = latitude;
	}




	public float getLongitude() {
		return longitude;
	}




	public void setLongitude(float longitude) {
		this.longitude = longitude;
	}




	public List<Monument> getMonuments() {
		return monuments;
	}




	public void setMonuments(List<Monument> monuments) {
		this.monuments = monuments;
	}




	@Override
	public String toString() {
		return String.format ("%s %s", code_insee, dep,nom_commune, latitude, longitude, monuments);
	}
	
	

	
	
	

}

