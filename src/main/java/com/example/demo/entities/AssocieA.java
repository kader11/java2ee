package com.example.demo.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="associeA")
public class AssocieA implements Serializable {
	
	@Id
	@ManyToOne
	@JoinColumn(name="codeM", referencedColumnName="codeM")
	private Monument codeM;
	

	@Id
	@ManyToOne
	@JoinColumn(name="num_celebrite", referencedColumnName="num_celebrite")
	private Celebrite num_celebrite;


	public AssocieA() {}


	public AssocieA(Monument codeM, Celebrite num_Celebrite) {
		this.codeM = codeM;
		this.num_celebrite = num_Celebrite;
	}


	
	
	public Monument getCodeM() {
		return codeM;
	}
	public void setCodeM(Monument codeM) {
		this.codeM = codeM;
	}

	

	public Celebrite getNum_Celebrite() {
		return num_celebrite;
	}
	public void setNum_Celebrite(Celebrite num_Celebrite) {
		this.num_celebrite = num_Celebrite;
	}
		

	
	
	
	
	

}
