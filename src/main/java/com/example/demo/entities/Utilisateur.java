package com.example.demo.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;



	@Entity
	@Table(name = "Utilsateur")
	public class Utilisateur  implements Serializable{
		
		@Id	
		private String email;
		@Column(name ="nom")
	    private String nom;
		@Column(name = "prenom")
	    private String prenom;
		@Column(name ="password")
	    private String password;
		@Column(name ="role")
		private String role;
		
		
		public Utilisateur(){}
		
		public Utilisateur(String email, String nom, String prenom, String password, String role) {
			this.email = email;
			this.nom = nom;
			this.prenom = prenom;
			this.password = password;
			this.role = role;
		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		public String getNom() {
			return nom;
		}

		public void setNom(String nom) {
			this.nom = nom;
		}

		public String getPrenom() {
			return prenom;
		}

		public void setPrenom(String prenom) {
			this.prenom = prenom;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

		public String getRole() {
			return role;
		}

		public void setRole(String role) {
			this.role = role;
		}
		

		@Override
		public String toString() {
			return "email= " + email + ", nom= " + nom + ", prenom= " + prenom + ", password= " + password
					+ ", rôle= " + role ;
		}
		
		
		
		
		

	}

