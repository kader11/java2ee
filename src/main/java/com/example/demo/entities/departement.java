package com.example.demo.entities;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity	//pour dire que cette classe represente une table
@Table(name = "departement") // pour dire que cette table corespond a la table departement
public class departement implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	private Integer dep;
		
	@Column (length = 46)
	private String chef_lieu;
		
	@Column (name= "nom_dep", length=30)
	private String nom_dep;
	
	@Column (name= "numreg", length=4)
	private String numreg;
	
	
	
	@OneToMany(mappedBy ="dep", fetch=FetchType.LAZY)
	private Collection<Lieu> lieu;
	
	public departement() {}


	public departement(Integer dep, String chef_lieu, String nom_dep, String numreg) {
		this.dep = dep;
		this.chef_lieu = chef_lieu;
		this.nom_dep = nom_dep;
		this.numreg = numreg;
	}


	public Integer getDep() {
		return dep;
	}


	public void setDep(Integer dep) {
		this.dep = dep;
	}


	public String getChef_lieu() {
		return chef_lieu;
	}


	public void setChef_lieu(String chef_lieu) {
		this.chef_lieu = chef_lieu;
	}


	public String getNom_dep() {
		return nom_dep;
	}


	public void setNom_dep(String nom_dep) {
		this.nom_dep = nom_dep;
	}


	public String getNumreg() {
		return numreg;
	}


	public void setNumreg(String numreg) {
		this.numreg = numreg;
	}


	@Override
	public String toString() {
		return ""+dep;
	}
		
	
}