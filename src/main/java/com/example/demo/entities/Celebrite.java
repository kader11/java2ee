package com.example.demo.entities;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
//@Table(name = "Celebrite")
public class Celebrite implements Serializable{
	private static final long serialVersionUID=1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer num_Celebrite;
	
	@Column (name = "epoque")
	private String epoque;
	
	@Column (name = "nationalite")
	private String nationalite ;
	
	@Column (name = "nom")
	private String  nom;
	
	@Column (name = "prenom")
	private String prenom;
	
	public Celebrite() {
		super();
	}
	public Celebrite(Integer num_Celebrite, String epoque, String nationalite, String nom, String prenom) {
		super();
		this.num_Celebrite = num_Celebrite;
		this.epoque = epoque;
		this.nationalite = nationalite;
		this.nom = nom;
		this.prenom = prenom;
	}

	public Integer getNum_Celebrite() {
		return num_Celebrite;
	}

	public void setNum_celebrite(Integer num_celebrite) {
		this.num_Celebrite = num_Celebrite;
	}

	public String getEpoque() {
		return epoque;
	}

	public void setEpoque(String epoque) {
		this.epoque = epoque;
	}

	public String getNationalite() {
		return nationalite;
	}

	public void setNationalite(String nationalite) {
		this.nationalite = nationalite;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

/*	@Override
	public String toString() {
		return "num_Celebrite=" + num_Celebrite + ", epoque=" + epoque + ", nationalite=" + nationalite
				+ ", nom=" + nom + ", prenom=" + prenom ;
	}

	public static void save(Celebrite celAAjouter) {
		// TODO Auto-generated method stub
		*/
	}
	

