package com.example.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.example.demo.entities.Monument;
@Repository
@Transactional
@Service
public class MonumentDAO implements EntityRepository <Monument> {
	
	@PersistenceContext
	private EntityManager em;
	
	@Override
	public List<Monument> findAll() {
		// TODO Auto-generated method stub
		Query req=em.createQuery("select m from Monument m");
		return req.getResultList();
	}
	
	
	
	/*
	@Override
	public Monument get(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}
*/
	@Override
	public Monument save(Monument m ) {
		em.persist(m);
		return m;
	}

	

	@Override
	public List<Monument> findByDesignation(String m) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void remove(Monument a) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void supp(Integer a) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Monument update(Monument a) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void supp(String a) {
		// TODO Auto-generated method stub
		
	}

}
